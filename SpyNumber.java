public class SpyNumber {
    public static void main(String[] args) {
        int num = 1124; // Change this to the number you want to check
        int sum = 0;
        int product = 1;
        while (num > 0) {
            int digit = num % 10;
            sum += digit;
            product *= digit;
            num /= 10;
        }
        if (sum == product) {
            System.out.println(" is a Spy number.");
        } else {
            System.out.println(" is not a Spy number.");
        }
    }
}
