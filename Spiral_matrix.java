package LearnArray;

public class Spiral_matrix {
	public static void main(String[] args) {
		int n = 5;
		int rowstart = 0;
		int rowend = n - 1;
		int colstart = 0;
		int colend = n - 1;
		int[][] arr = new int[n][n];
		int value = 1;

		while (rowstart <= rowend && colstart <= colend) {
			// Moving left to right
			for (int c = colstart; c <= colend; c++)
				arr[rowstart][c] = value++;

			// Move down
			for (int r = rowstart + 1; r <= rowend; r++)
				arr[r][colend] = value++;

			// Move right to left and move up
			if (rowstart < rowend && colstart < colend) {
				// Move right to left
				for (int c = colend - 1; c > colstart; c--)
					arr[rowend][c] = value++;

				// Move up
				for (int r = rowend; r > rowstart; r--)
					arr[r][colstart] = value++;
			}

			rowstart++;
			rowend--;
			colstart++;
			colend--;
		}

		// Print the matrix
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(arr[i][j] + "\t");
			}
			System.out.println();
		}
	}
}
