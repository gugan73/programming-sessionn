package Programming_session;

import java.util.Scanner;

public class Factorial_num {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.print("Enter the Number : ");
	int number=sc.nextInt();
	int result=find_fact(number);
	System.out.println("Factorial of " + number + " is: "+result);
}

private static int find_fact(int no) {
	if(no==1)return no;
	return no*find_fact(no-1);
}
}
