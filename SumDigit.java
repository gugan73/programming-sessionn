/* TO PRUINT A SUM OF DIGITS UNTILL IT BECOME  A SINGLE DIGIT */
public class SumDigit {
    // Create an instance of SumDigit
   // private static SumDigit sd; the object acn be declared like this also

    public static void main(String args[]) {
        // Initialize an instance of SumDigit and store it in the variable sd
        SumDigit sd = new SumDigit();
        // Calculate the sum of digits for the number 1234
        int result = sd.find_sum(12345);
        // Check if the result is greater than 9
        if (result>9)
		{
            // If greater than 9, print the result
            System.out.println(result);
        } else {
            // If not greater than 9, calculate the sum of digits again
            result = sd.find_sum(result);
            // Print the final result, which is expected to be a single-digit number
            System.out.println(result);
        }
    }

    // Method to find the sum of digits of a given number
    public int find_sum(int no) {
        int sum = 0;
        // Iterate through each digit of the number
        while (no > 0) {
            int rem = no % 10; // Extract the last digit 4
            sum = sum + rem;   // Add the digit to the sum 0+4
            no = no / 10;      // Remove the last digit  123
        }
        return sum; // Return the final sum 
    }
}
