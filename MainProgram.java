// program1
class  Whileloop
{
public static void main (String args[])
{
int no=1;
	while(no<=5)
	{
	System.out.println(no+" ");
		no=no+1;
	}
	System.out.println("TO PRINT  2 TO  8 EVEN NUMBERS");
}
}

// program2
class  Whileloop1
{
public static void main (String args[])
{
int no=2;
	while(no<=10)
	{
	System.out.println(no+" ");
		no=no+2;
	}
	System.out.println("TO PRINT 3 TO 15 ODD NUMBERS ");
}
}

//program3
	class  Whileloop2
{
public static void main (String args[])
{
int no=3;
	while(no<=15)
	{
	System.out.println(no+" ");
		no=no+3;
	}	
	System.out.println("TO PRINT 1 TO 10 NUMBERS");
}
}
//program4
	class  Whileloop3
{
public static void main (String args[])
{
int no=1;
	while(no<=10)
	{
	System.out.println(no+" ");
		no=no+1;
	}	
}
}

// MainProgram.java
public class MainProgram {
    public static void main(String[] args) {
        // Run Program 1
        Whileloop.main(args);

        // Run Program 2
        Whileloop1.main(args);
		// run program3
		Whileloop2.main(args);
		// run program4
		Whileloop3.main(args);
    }
}
