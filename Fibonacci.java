// FIBONACCI SERIES WITHOUT THIRD VARIABLE
public class Fibonacci
{
public static void main (String[] args)
{
int a=0,b=1;
while(a<15)
{
System.out.println(a);
int c=a+b;
a=b;
b=c;
}
}
}

/* FIBONACCI SERIES WITHOUT THIRD VARIABLE
public class Fibonacci
{
public static void main (String[] args)
{
int a=0,b=1;
while(a<15)
{
System.out.println(a);
a=b-a;
b=a+b;
}
}
}
*/