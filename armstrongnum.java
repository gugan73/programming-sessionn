public class armstrongnum {
    public static void main(String[] args) {
        // Change the value of num to the number you want to check
        int num =92727;
        
        int originalNum = num;
        int sum = 0;

        // Calculate the number of digits in the given number
        int numOfDigits = (int) Math.log10(num) + 1;

        // Calculate sum of nth power of each digit
        while (num > 0) {
            int digit = num % 10;
            sum += Math.pow(digit, numOfDigits);
            num /= 10;
        }

        // Check if the number is an Armstrong number
        if (originalNum == sum) {
            System.out.println(originalNum + " is an Armstrong number.");
        } else {
            System.out.println(originalNum + " is not an Armstrong number.");
        }
    }
}
