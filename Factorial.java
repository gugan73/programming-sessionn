import java.math.BigInteger;

public class Factorial {
    public static void main(String[] args) {
        int number = 100;
        BigInteger result = calculateFactorial(number);

        System.out.println("Factorial of " + number + " is:");
        System.out.println(result);
    }

    private static BigInteger calculateFactorial(int n) {
        BigInteger result = BigInteger.ONE;
        for (int i = 1; i <= n; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }
}
